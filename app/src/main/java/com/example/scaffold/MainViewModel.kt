package com.example.scaffold

import android.app.Application
import android.content.Intent
import android.util.Log
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewModelScope
import com.blankj.utilcode.util.ActivityUtils
import com.example.scaffold.dialog.ShowDemoFragment
import com.example.scaffold.list.ListActivity
import com.example.scaffold.net.ApiService
import com.example.scaffold.viewPage.ViewPagerActivity
import com.hzb.scaffold.base.BaseModel
import com.hzb.scaffold.base.BaseViewModel
import com.hzb.scaffold.bus.Messenger
import com.hzb.scaffold.databinding.bindAction
import com.hzb.scaffold.extension.launch
import com.hzb.scaffold.extension.toGson
import com.hzb.scaffold.extension.toastShort
import com.hzb.scaffold.net.RetrofitManager
import com.hzb.scaffold.net.glide.ImageUrl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    val textInfo = ObservableField<String>("点击我")

    val imageUrl = ObservableField<ImageUrl>(
        ImageUrl(
            "111",
            "https://th.bing.com/th?id=OIP.2rQ25qnSMQHXGcHz3Rp2pAHaEo&w=316&h=197&c=8&rs=1&qlt=90&o=6&pid=3.1&rm=2"
        )
    )

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
    }

    val onClickCommand = bindAction {
        viewModelScope.launch(Dispatchers.Main) {
            Log.i("hzb", "当前线程:${Thread.currentThread().name}")
            textInfo.set("当前线程:${Thread.currentThread().name}")
        }
    }

    val onGotoListPage = bindAction {
        ActivityUtils.startActivity(Intent(application.baseContext, ListActivity::class.java))
    }

    val onGotoViewPager = bindAction {
        ActivityUtils.startActivity(Intent(application.baseContext, ViewPagerActivity::class.java))
    }

    val onFouseCommand = bindAction<Boolean> {
        toastShort { "${it}" }
    }


    val gotoDialogCommand= bindAction {
        ShowDemoFragment().show((ActivityUtils.getTopActivity() as FragmentActivity).supportFragmentManager,"11")
    }

    val apiClickCommand = bindAction {
        launch({
            val hostApi = RetrofitManager.instance.create(ApiService::class.java).getHostApi()
            val sucess = hostApi.isSuccessful
            if (sucess) {
                toastShort { hostApi.body()?.data?.last().toGson() }
            }
        })
    }


    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        Log.i("hzb", "onCreate")
        launch({
            delay(3000)
            Messenger.getDefault().send("123")
            Messenger.getDefault().send(456)
        })
    }

    override fun onResume(owner: LifecycleOwner) {
        super.onResume(owner)
        Log.i("hzb", "onResume")
    }
}