package com.example.scaffold

import com.blankj.utilcode.util.CrashUtils
import com.hzb.scaffold.ScaffoldConfig
import com.hzb.scaffold.base.BaseVMApplication
import com.hzb.scaffold.extension.logcate
import java.io.File
import kotlin.concurrent.thread

class MyApplication: BaseVMApplication() {


    override fun onCreate() {
        super.onCreate()
    }

    override fun appScaffoldConfig(scaffoldConfig: ScaffoldConfig) {
        scaffoldConfig.apply {
            this.hostUrl ="https://www.baidu.com"
            this.isDebug=true
        }
    }

    override fun logUploadServer(dir: String) {

    }
}