package com.example.scaffold.net

import com.hzb.scaffold.net.annotation.BaseUrl
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Tag
import retrofit2.http.Url

interface ApiService {

    @GET("/hotkey/json")
    @BaseUrl("https://www.wanandroid.com")
    suspend fun getHostApi():Response<HostResponse>
}