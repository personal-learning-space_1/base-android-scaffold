package com.example.scaffold.net

import java.io.Serializable


data class HostResponse(val data: List<HostBean>,val errorCode:Int,val errorMsg:String): Serializable
data class HostBean(val name:String,val courseId:Int): Serializable
