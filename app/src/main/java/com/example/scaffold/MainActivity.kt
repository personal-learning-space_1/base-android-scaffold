package com.example.scaffold

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.Utils
import com.example.scaffold.databinding.ActivityMainBinding
import com.example.scaffold.fragment.FragmentDemoActivity
import com.example.scaffold.net.ApiService
import com.hzb.scaffold.BR
import com.hzb.scaffold.base.BaseVMActivity
import com.hzb.scaffold.base.BaseVMApplication
import com.hzb.scaffold.extension.launch
import com.hzb.scaffold.extension.logcate
import com.hzb.scaffold.extension.parseString
import com.hzb.scaffold.extension.toGson
import com.hzb.scaffold.extension.toastShort
import com.hzb.scaffold.net.RetrofitManager
import com.hzb.scaffold.permission.PermissionLauncher

class MainActivity : BaseVMActivity<MainViewModel, ActivityMainBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTopBarText("我是黄智波")
        setRightText("更多")
        AppUtils.registerAppStatusChangedListener(object : Utils.OnAppStatusChangedListener {
            override fun onForeground(p0: Activity?) {
                logcate { "前台" }
            }

            override fun onBackground(p0: Activity?) {
                logcate { "后台" }
            }

        })

        findViewById<View>(R.id.gotoFragment).setOnClickListener {
            startActivity(Intent(this, FragmentDemoActivity::class.java))
        }
        findViewById<View>(R.id.gotoStatusView).setOnClickListener {
            startActivity(Intent(this, StatusViewActivity::class.java))
        }
        findViewById<View>(R.id.permission).setOnClickListener {
            PermissionLauncher.requestPermission(arrayOf(Manifest.permission.CAMERA),
                "相机使用,我们使用此功能来记录你的微信第聚合物我款到发货看上的花费多少的",
                {
                    toastShort { "相机权限已开启" }
                }, {
                    toastShort { "相机权限拒绝" }
                })
        }
        findViewById<View>(R.id.api).setOnClickListener {

        }
    }

    override fun clickRightEvent() {
        super.clickRightEvent()
        viewModel.statusModel.showError()
    }

    override fun initViewAfter() {

    }

    override fun onPause() {
        super.onPause()
    }


    override fun getContentId(): Int {
        return R.layout.activity_main
    }

    override fun getViewModelBR(): Int {
        return BR.viewModel
    }

    override fun childToEdge(): Boolean {
        return false
    }

    override fun onRetryStatusEvent(view: View) {
        super.onRetryStatusEvent(view)
    }
}