package com.example.scaffold.viewPage

import com.example.scaffold.BR
import com.example.scaffold.R
import com.example.scaffold.databinding.ActivityViewpagerLayoutBinding
import com.hzb.scaffold.base.BaseVMActivity

class ViewPagerActivity : BaseVMActivity<ViewPagerViewModel, ActivityViewpagerLayoutBinding>() {
    override fun initViewAfter() {
       // TODO("Not yet implemented")
    }

    override fun getContentId(): Int = R.layout.activity_viewpager_layout

    override fun getViewModelBR(): Int = BR.viewModel
}