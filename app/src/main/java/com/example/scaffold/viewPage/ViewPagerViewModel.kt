package com.example.scaffold.viewPage

import android.app.Application
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableInt
import androidx.lifecycle.LifecycleOwner
import com.example.scaffold.BR
import com.example.scaffold.R
import com.hzb.scaffold.base.BaseModel
import com.hzb.scaffold.base.BaseViewModel
import com.hzb.scaffold.databinding.bindAction
import com.hzb.scaffold.extension.launch
import com.hzb.scaffold.extension.toastShort
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding
import kotlinx.coroutines.delay

class ViewPagerViewModel(application: Application) : BaseViewModel<BaseModel>(application) {

    val currentIndex = ObservableInt(1)

    val items = ObservableArrayList<ItemViewModel>()

    val itemBind = ItemBinding.of<ItemViewModel>(BR.viewModel,R.layout.viewpager_item_layout)

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        items.add(ItemViewModel("首页"))
        items.add(ItemViewModel("我的页面"))
        items.add(ItemViewModel("购物车页面"))
    }


    class ItemViewModel(val title:String){
        val onclickCommand = bindAction {
            toastShort { title }
        }
    }
}