package com.example.scaffold.list

import android.annotation.SuppressLint
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.scaffold.BR
import com.example.scaffold.R
import com.example.scaffold.databinding.ActivityListLayoutBinding
import com.hzb.scaffold.base.BaseVMActivity
import com.hzb.scaffold.extension.logcati

class ListActivity: BaseVMActivity<ListViewModel, ActivityListLayoutBinding>() {
    @SuppressLint("ClickableViewAccessibility")
    override fun initViewAfter() {
        binding.recyclerView.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_MOVE){
               val layoutManager= binding.recyclerView.layoutManager as LinearLayoutManager
               val isVerticall= layoutManager.canScrollVertically()
                val visibleItemCount = layoutManager.childCount
               val ishorizon= layoutManager.canScrollHorizontally()
               val lastId=  layoutManager.findLastVisibleItemPosition()
                logcati { "索引:${lastId} 一屏显示的数量:${visibleItemCount}===>纵向:${isVerticall} 横向:${ishorizon}" }
            }
            false
        }
    }

    override fun getContentId(): Int =R.layout.activity_list_layout

    override fun getViewModelBR(): Int =BR.viewModel
}