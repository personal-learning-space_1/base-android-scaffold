package com.example.scaffold.list

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableList
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import com.example.scaffold.BR
import com.example.scaffold.R
import com.hzb.scaffold.base.BaseListViewModel
import com.hzb.scaffold.base.BaseModel
import com.hzb.scaffold.base.BaseViewModel
import com.hzb.scaffold.base.paging.PageIndexLoad
import com.hzb.scaffold.base.paging.RefreshStatus
import com.hzb.scaffold.base.status.StatusInfo
import com.hzb.scaffold.bus.Messenger
import com.hzb.scaffold.databinding.BindCommand
import com.hzb.scaffold.databinding.collections.DiffObservableArrayList
import com.hzb.scaffold.extension.launch
import com.hzb.scaffold.extension.logcati
import com.hzb.scaffold.extension.toastShort
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding
import kotlinx.coroutines.delay
import kotlin.jvm.internal.Intrinsics.Kotlin


class ListViewModel(application: Application) : BaseListViewModel<BaseModel, String>(application) {
    override fun itemCallback(): DiffUtil.ItemCallback<String> {
        return object : DiffUtil.ItemCallback<String>() {
            override fun areItemsTheSame(p0: String, p1: String): Boolean {
                return p0==p1
            }

            override fun areContentsTheSame(p0: String, p1: String): Boolean {
                return true
            }

        }
    }


    override fun getItemBinding(): ItemBinding<String> =
        ItemBinding.of<String>(BR.item, R.layout.layout_item)

    override fun loadData(index: Int, loadResult: PageIndexLoad.LoadResult<String>) {
        launch({
            toastShort { "当前刷新状态:${refreshStatus.get()},分页索引:${index}" }
            delay(1000)
            if (index!=0){
                loadResult.success(
                    arrayListOf(
                        "我是新增数据1",
                        "我是新增数据1",
                        "我是新增数据1",
                        "我是新增数据1",
                        "我是新增数据1",
                    ),index,10)
                return@launch
            }
            loadResult.success(
                arrayListOf(
                    "的范德萨发收到的氛围",
                    "ddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "dddddddddddd",
                    "ddddd"
                ), index, 10
            )
        })
    }

    override fun firstIndex(): Any? {
        return super.firstIndex()
    }
    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        statusModel.showLoading(StatusInfo(title = "加载中..."))
        Messenger.getDefault().register(this,String::class.java){ it->
            toastShort { it }
        }

        Messenger.getDefault().register(this,Integer::class.java){ it->
            toastShort { "数字:${it}" }
        }
    }
}