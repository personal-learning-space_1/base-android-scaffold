package com.example.scaffold.fragment

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.scaffold.R
import com.hzb.scaffold.extension.toastLong
import com.hzb.scaffold.extension.toastShort

class FragmentDemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment_demo)
        val fragment = MineFragment()
        val transient = supportFragmentManager.beginTransaction()
        transient.add(R.id.content,fragment)
        transient.commitNow()
    }
}