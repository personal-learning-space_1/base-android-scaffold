package com.example.scaffold.fragment

import com.example.scaffold.BR
import com.example.scaffold.MainViewModel
import com.example.scaffold.R
import com.example.scaffold.databinding.FragmentMineLayoutBinding
import com.hzb.scaffold.base.BaseVMFragment

class MineFragment: BaseVMFragment<MainViewModel, FragmentMineLayoutBinding>() {
    override fun initViewAfter() {
       // viewModel.statusModel.showLoading()
    }

    override fun getContentId(): Int = R.layout.fragment_mine_layout

    override fun getViewModelBR(): Int =BR.viewModel
}