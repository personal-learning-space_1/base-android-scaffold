package com.example.scaffold.dialog

import com.example.scaffold.BR
import com.example.scaffold.MainViewModel
import com.example.scaffold.R
import com.example.scaffold.databinding.LayoutDemoDialogBinding
import com.hzb.scaffold.base.BaseVMDialogFragment

class ShowDemoFragment: BaseVMDialogFragment<MainViewModel, LayoutDemoDialogBinding>() {
    override fun getContentId(): Int {
        return R.layout.layout_demo_dialog
    }

    override fun getViewModelBR(): Int {
        return BR.viewModel
    }

    override fun initViewAfter() {
    }
}