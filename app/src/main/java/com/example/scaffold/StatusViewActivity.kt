package com.example.scaffold

import com.example.scaffold.databinding.ActivityStatusViewBinding
import com.hzb.scaffold.base.BaseModel
import com.hzb.scaffold.base.BaseVMActivity
import com.hzb.scaffold.base.BaseViewModel
import com.hzb.scaffold.base.status.StatusInfo
import com.hzb.scaffold.extension.launch
import kotlinx.coroutines.delay

class StatusViewActivity : BaseVMActivity<BaseViewModel<BaseModel>, ActivityStatusViewBinding>() {

    override fun initViewAfter() {
       viewModel.statusModel.showLoading(StatusInfo(info = "数据加载中..."))

        viewModel.launch({
            delay(2000)
            viewModel.statusModel.showNormal()
        })
    }

    override fun getContentId(): Int=R.layout.activity_status_view

    override fun getViewModelBR(): Int =BR.viewModel
}