package com.hzb.scaffold.base

import android.app.Application
import androidx.recyclerview.widget.DiffUtil
import com.hzb.scaffold.base.paging.BasePagingViewModel
import com.hzb.scaffold.base.paging.PageIndexLoad
import com.hzb.scaffold.base.paging.PagingDataSource
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding

abstract class BaseListViewModel<M : BaseModel, ITEM : Any>(application: Application) :
    BasePagingViewModel<M, ITEM>(
        application
    ) {

    protected abstract fun itemCallback(): DiffUtil.ItemCallback<ITEM>

    /**
     * Desc: RecyclerView的ItemBinding
     * <p>
     */
    abstract fun getItemBinding(): ItemBinding<ITEM>

    abstract fun loadData(index: Int, loadResult: PageIndexLoad.LoadResult<ITEM>)

    override fun createDataSource(): PagingDataSource<ITEM> {
        return object : PageIndexLoad<ITEM>(itemCallback(), { finishLoad(it) }) {
            override fun loadData(index: Int, loadResult: LoadResult<ITEM>) {
                this@BaseListViewModel.loadData(index, loadResult)
            }

            override fun defalutIndex(): Int {
                return firstIndex() as Int
            }
        }
    }


    override fun firstIndex(): Any? {
        return 0
    }
}