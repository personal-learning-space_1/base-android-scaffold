package com.hzb.scaffold.base.status

import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import android.view.ViewGroup.MarginLayoutParams
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import com.hzb.scaffold.ScaffoldConfig

class StatusViewControl(val rootView: ViewGroup, val top: Int = 0) {

    val status by lazy { ScaffoldConfig.statusView }

    private lateinit var statusView: View

    fun addStatusViewIfNeed() {
        val find = rootView.findViewWithTag<View>("IStatusView")
        if (find != null) return
        statusView = status.onCreateView(rootView.context)
        //防止事件透传
        statusView.isClickable = true
        statusView.setTag("IStatusView")
        addViewToParentType(rootView)
    }


    fun removeStatusView() {
        rootView.removeView(statusView)
    }


    private fun addViewToParentType(rootView: ViewGroup) {
        when (rootView) {
            is ConstraintLayout -> {
                val layoutParams = ConstraintLayout.LayoutParams(
                    LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT
                )
                layoutParams.topMargin = top
                layoutParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams.leftToLeft = ConstraintLayout.LayoutParams.PARENT_ID
                layoutParams.rightToRight = ConstraintLayout.LayoutParams.PARENT_ID
                rootView.addView(statusView, layoutParams)
            }

            is LinearLayout -> {
                throw Exception("***********使用省缺页功能,父布局不能是LinearLayout,请检查页面xml布局************")
            }

            else -> {
                val layoutParams =
                    MarginLayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
                layoutParams.topMargin = top
                rootView.addView(statusView, layoutParams)
            }
        }
    }
}