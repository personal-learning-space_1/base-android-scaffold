package com.hzb.scaffold.base

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.hzb.scaffold.R
import com.hzb.scaffold.utils.ReflectUtil

/**
 * dialog 基类
 * @author zhibo.huang
 * @date   2024/7/31
 */
abstract class BaseVMDialogFragment<VM : BaseViewModel<BaseModel>, VB : ViewDataBinding> :
    DialogFragment() {

    //viewModel
    protected lateinit var viewModel: VM

    //布局持有
    protected lateinit var binding: VB


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.BaseCustomDialog)
        createViewModel()
        initObserverEvent()
    }

    fun initObserverEvent(){
        viewModel.uiBaseEvent.backEvent.observe(this){
            this.dismiss()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate<VB>(inflater, getContentId(), container, false)
        binding.setVariable(getViewModelBR(), viewModel)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setLayout(getLayoutParams().width, getLayoutParams().height)
        dialog?.window?.attributes?.gravity = getGravity()
        initViewAfter()
    }


    private fun createViewModel() {
        var clazzVM: Class<VM>
        try {
            clazzVM = ReflectUtil.getGenericityClass<VM>(this, 0)
        } catch (e: Exception) {
            clazzVM = BaseViewModel::class.java as Class<VM>
        }
        val vmFactory = createVMFactory()
        viewModel = if (vmFactory == null) {
            ViewModelProvider(this)[clazzVM]
        } else {
            ViewModelProvider(this, vmFactory)[clazzVM]
        }
        this.lifecycle.addObserver(viewModel)
    }


    /**
     * 如果viewModel构造函数有需要额外传参,可以重新此方法,
     * 实现参考注释代码:
     * return object :ViewModelProvider.Factory{
     *             override fun <T : ViewModel> create(modelClass: Class<T>): T {
     *                return MainViewModel("id") as T
     *             }
     *         }
     * @author zhibo.huang
     * @date 2024/5/9
     */
    open fun createVMFactory(): ViewModelProvider.Factory? {
        return null
    }


    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

    /**
     * 返回布局id
     *
     * @return
     */
    abstract fun getContentId(): Int

    /**
     * @Description:返回databinding中绑定参数索引
     * @author zhibo.huang
     * @date 2024/5/9
     */
    abstract fun getViewModelBR(): Int

    abstract fun initViewAfter()

    /**
     * 布局位置
     * @return:
     * @Author: zhibo.huang
     * @date:   2024/7/31
     */
    open fun getGravity(): Int = Gravity.CENTER

    /**
     * 布局宽高
     * @return:
     * @Author: zhibo.huang
     * @date:   2024/7/31
     */
    open fun getLayoutParams(): LayoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
}