package com.hzb.scaffold.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.SystemBarStyle
import androidx.activity.enableEdgeToEdge
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.hzb.scaffold.R
import com.hzb.scaffold.ScaffoldConfig
import com.hzb.scaffold.base.status.StatusType
import com.hzb.scaffold.base.status.StatusViewControl
import com.hzb.scaffold.base.toobar.ClickLocation
import com.hzb.scaffold.bus.Messenger
import com.hzb.scaffold.utils.ReflectUtil

/**
 * @Description:基于mvvm封装的基类activity
 * @author zhibo.huang
 * @date 2024/4/24
 */
abstract class BaseVMActivity<VM : BaseViewModel<BaseModel>, VB : ViewDataBinding> :
    AppCompatActivity() {
    //viewModel
    protected lateinit var viewModel: VM

    //布局持有
    protected lateinit var binding: VB

    private val toobarView by lazy { ScaffoldConfig.toobarView }
    //省缺页控制类
    private val statusControl by lazy {
        getStatusViewControl().apply {
            this.status.setRetryListener(::onRetryStatusEvent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewBefore()
        createViewModel()
        createDataBinding()
        layoutConfig()
        initObserverEvent()
        initViewAfter()
    }

    open fun initObserverEvent() {
        viewModel.uiBaseEvent.backEvent.observe(this) {
            finish()
        }
        viewModel.statusModel.statusSwitch.observe(this) {
            statusControl.addStatusViewIfNeed()
            when (it.first) {
                StatusType.Empty -> {
                    statusControl.status.showEmpty(it.second)
                }

                StatusType.Error -> {
                    statusControl.status.showError(it.second)
                }

                StatusType.Loading -> {
                    statusControl.status.showLoading(it.second)
                }

                StatusType.Normal -> {
                    statusControl.removeStatusView()
                }
            }
        }
    }

    /**
     * 布局配置
     * @author:  zhibo.huang
     * @date:  2024/5/11
     */
    private fun layoutConfig() {
        if (!childToEdge()) {
            ViewCompat.setOnApplyWindowInsetsListener(binding.root.parent as ViewGroup) { v, insets ->
                //获取状态栏信息,动态设置根布局的padding
                val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
                v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
                insets
            }
        }
    }

    /**
     * 布局初始化之前调用
     * @author zhibo.huang
     * @date 2024/5/9
     */
    open fun initViewBefore() {
        if (getBarStyle() != null) {
            enableEdgeToEdge(getBarStyle()!!, getBarStyle()!!)
        } else {
            enableEdgeToEdge()
        }
    }

    /**
     * 省缺页重试点击事件
     * @author zhibo.huang
     * @date 2024/5/16
     */
    open fun onRetryStatusEvent(view: View) {
        viewModel.statusModel.showLoading()
    }


    /**
     * @Description: 状态栏默认是light模式(白色黑字)
     * 栗子:SystemBarStyle.light(Color.WHITE, Color.WHITE)
     * @param null
     * @return:
     * @author:  zhibo.huang
     * @date:  2024/5/9
     */
    open fun getBarStyle(): SystemBarStyle? = null


    abstract fun initViewAfter()

    /**
     * databinding相应初始化
     * @author zhibo.huang
     * @date 2024/5/9
     */
    private fun createDataBinding() {
        val rootView =
            LayoutInflater.from(this).inflate(R.layout.activity_base_layout, null) as ViewGroup
        //添加toobar
        if (needToobar()) {
            rootView.addView(
                toobarView.createView(this),
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            toobarView.clickCommand {
                when (it) {
                    ClickLocation.Left -> finish()
                    ClickLocation.Right -> clickRightEvent()
                }
            }
        }
        binding =
            DataBindingUtil.inflate<VB>(LayoutInflater.from(this), getContentId(), rootView, false)
        rootView.addView(
            binding.root,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        setContentView(rootView)
        binding.setVariable(getViewModelBR(), viewModel)
    }


    /**
     * 如果viewModel构造函数有需要额外传参,可以重新此方法,
     * 实现参考注释代码:
     * return object :ViewModelProvider.Factory{
     *             override fun <T : ViewModel> create(modelClass: Class<T>): T {
     *                return MainViewModel("id") as T
     *             }
     *         }
     * @author zhibo.huang
     * @date 2024/5/9
     */
    open fun createVMFactory(): ViewModelProvider.Factory? {
        return null
    }


    private fun createViewModel() {
        var clazzVM:Class<VM>
        try {
            clazzVM = ReflectUtil.getGenericityClass<VM>(this, 0)
        }catch (e:Exception){
            clazzVM = BaseViewModel::class.java as Class<VM>
        }
        val vmFactory = createVMFactory()
        viewModel = if (vmFactory == null) {
            ViewModelProvider(this)[clazzVM]
        } else {
            ViewModelProvider(this, vmFactory)[clazzVM]
        }
        this.lifecycle.addObserver(viewModel)
    }


    override fun onDestroy() {
        super.onDestroy()
        //解除Messenger注册
        Messenger.getDefault().unregister(viewModel)
        //databind 解绑
        binding.unbind()
    }


    protected fun setRightText(title: String, @ColorRes color: Int? = null) {
        toobarView.setRightText(title, color)
    }

    protected fun setTopBarText(title: String, @ColorRes color: Int? = null) {
        toobarView.setTitleText(title, color)
    }

    protected fun setLeftIcon(@DrawableRes icon: Int) {
        toobarView.setLeftIcon(icon)
    }

    protected fun setRightIcon(@DrawableRes icon: Int) {
        toobarView.setRightIcon(icon)
    }

    protected fun getStatusViewControl(): StatusViewControl {
        return StatusViewControl(binding.root as ViewGroup, 0)
    }

    /**
     * 返回布局id
     *
     * @return
     */
    abstract fun getContentId(): Int

    /**
     * @Description:返回databinding中绑定参数索引
     * @author zhibo.huang
     * @date 2024/5/9
     */
    abstract fun getViewModelBR(): Int

    /**
     * toobar 右上角点击事件
     * @author:  zhibo.huang
     * @date:  2024/5/13
     */
    open fun clickRightEvent() {}

    /**
     * 孩子布局延伸到全屏,占用状态栏
     * @author zhibo.huang
     * @date 2024/5/9
     */
    open fun childToEdge() = false

    /**
     * 默认显示toobar
     * @author zhibo.huang
     * @date 2024/5/13
     */
    open fun needToobar() = true


}