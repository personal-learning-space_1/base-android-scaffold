package com.hzb.scaffold.base.paging

import android.app.Application
import androidx.databinding.ObservableInt
import androidx.lifecycle.LifecycleOwner
import com.hzb.scaffold.base.BaseModel
import com.hzb.scaffold.base.BaseViewModel
import com.hzb.scaffold.databinding.bindAction
import java.util.concurrent.atomic.AtomicBoolean

/**
 * 支持列表加载更多,下拉刷新的viewModel
 * @author zhibo.huang
 * @date 2024/6/4
 */
abstract class BasePagingViewModel<M : BaseModel, ITEM : Any>(application: Application) :
    BaseViewModel<M>(application) {

    var isLoading = AtomicBoolean(false)

    /**
     * 加载更多状态
     */
    val loadStatus = ObservableInt(FooterStatus.STATUS_NONE)

    /**
     * 刷新状态
     */
    val refreshStatus = ObservableInt(RefreshStatus.STATUS_NONE)

    /**
     * 分页数据源
     */
    private val pageDataSource by lazy {
        createDataSource()
    }

    /**
     * Items数据
     */
    val items = pageDataSource.getItems()


    /**
     * 下拉刷新
     */
    val onRefreshCommand = bindAction {
        setRefreshStatus(RefreshStatus.STATUS_REFRESHING)
        pageDataSource.loadOnlineData(true)
    }

    /**
     * 上拉加载
     */
    val onLoadMoreCommand = bindAction {
        if (loadStatus.get() == FooterStatus.STATUS_LOADING
            || loadStatus.get() == FooterStatus.STATUS_NO_MORE
        ) {
            return@bindAction
        }
        setLoadMoreStatus(FooterStatus.STATUS_LOADING)
        pageDataSource.loadOnlineData(false)
    }

    override fun onCreate(owner: LifecycleOwner) {
        super.onCreate(owner)
        startLoadData(true)
    }

    /**
     * Desc: 重新加载数据
     * <p>
     * @param showDialog 是否显示加载中弹窗
     */
    fun callReload() {
        startLoadData(true)
    }

    /**
     * Desc: 设置刷新状态
     */
    private fun setRefreshStatus(@RefreshStatus status: Int) {
        if (enableRefresh()) {
            refreshStatus.set(status)
        }
    }

    /**
     * Desc: 设置上拉状态
     */
    private fun setLoadMoreStatus(@FooterStatus status: Int) {
        if (enableLoadMore()) {
            loadStatus.set(status)
        }
    }


    private fun startLoadData(refresh: Boolean) {
        synchronized(BasePagingViewModel::class.java) {
            if (isLoading.get()) {
                return@synchronized
            }
            isLoading.set(true)
            pageDataSource.loadOnlineData(refresh)
        }
    }

    /**
     * 加载结束时状态处理
     * @return:
     * @author:  zhibo.huang
     * @date:  2024/6/4
     */
    protected open fun finishLoad(error: Boolean) {
        isLoading.set(false)
        setRefreshStatus(RefreshStatus.STATUS_NONE)
        if (pageDataSource.noMore() && !isEmptyData()) {
            setLoadMoreStatus(FooterStatus.STATUS_NO_MORE)
        } else {
            if (error && (loadStatus.get() == FooterStatus.STATUS_LOADING || loadStatus.get() == FooterStatus.STATUS_FAILED)) {
                setLoadMoreStatus(FooterStatus.STATUS_FAILED)
            } else {
                setLoadMoreStatus(FooterStatus.STATUS_NONE)
            }
        }

        if (isEmptyData()) {
            if (error) {
                statusModel.showError()
            } else {
                statusModel.showEmpty()
            }
        } else {
            statusModel.showNormal()
        }
    }


    /**
     * Desc: 创建数据源
     * <p>
     */
    protected abstract fun createDataSource(): PagingDataSource<ITEM>

    /**
     * 支持加载更多
     * @return:
     * @author:  zhibo.huang
     * @date:  2024/6/4
     */
    protected open fun enableLoadMore() = true

    /**
     * 支持下拉刷新
     * @return:
     * @author:  zhibo.huang
     * @date:  2024/6/4
     */
    protected open fun enableRefresh() = true

    /**
     * Desc: 判断是否是空数据的条件，可重写
     * <p>
     */
    protected open fun isEmptyData() = items.isEmpty()

    /**
     * Desc: 分页索引第一个值
     * <p>
     */
    open fun firstIndex(): Any? = null


}