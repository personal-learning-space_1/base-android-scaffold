package com.hzb.scaffold.base.paging

import androidx.annotation.IntDef
import com.hzb.scaffold.base.paging.RefreshStatus.Companion.STATUS_FORBID
import com.hzb.scaffold.base.paging.RefreshStatus.Companion.STATUS_NONE
import com.hzb.scaffold.base.paging.RefreshStatus.Companion.STATUS_REFRESHING

/**
 * Desc: 刷新状态
 * <p>
 * author: linjiaqiang
 * Date: 2019/12/5
 */
@IntDef(STATUS_NONE, STATUS_REFRESHING, STATUS_FORBID)
@Retention(AnnotationRetention.SOURCE)
annotation class RefreshStatus {

    companion object {
        /**
         * 无刷新状态
         */
        const val STATUS_NONE = 0

        /**
         * 刷新中
         */
        const val STATUS_REFRESHING = 1

        /**
         * 禁止刷新
         */
        const val STATUS_FORBID = 2
    }

}