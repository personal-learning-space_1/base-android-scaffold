package com.hzb.scaffold.base.toobar

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.databinding.DataBindingUtil
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.Utils
import com.hzb.scaffold.R

/**
 * 默认toobar实现.如需自定义,可参考实现
 * @author zhibo.huang
 * @date 2024/5/13
 */
class DefalutToobarView : IToobarView() {

    lateinit var titleTv: TextView
    lateinit var rightTv: TextView

    lateinit var leftIcon: ImageView
    lateinit var rightIcon: ImageView

    @SuppressLint("MissingInflatedId")
    override fun createView(context: Context): View {
        val toobar = LayoutInflater.from(context).inflate(R.layout.layout_defalut_toobar, null)
        titleTv = toobar.findViewById<TextView>(R.id.title)
        leftIcon = toobar.findViewById<ImageView>(R.id.left)
        rightTv = toobar.findViewById<TextView>(R.id.right_text)
        rightIcon = toobar.findViewById<ImageView>(R.id.right)
        leftIcon.setOnClickListener {
            callback?.invoke(ClickLocation.Left)
        }

        rightTv.setOnClickListener {
            callback?.invoke(ClickLocation.Right)
        }

        rightTv.setOnClickListener {
            callback?.invoke(ClickLocation.Right)
        }
        return toobar
    }

    override fun setTitleText(title: String, @ColorRes color: Int?) {
        titleTv.setText(title)
        color?.let { titleTv.setTextColor(Utils.getApp().getColor(it)) }
    }

    override fun setLeftIcon(@DrawableRes icon: Int) {
        leftIcon.setImageResource(icon)
    }

    override fun setRightText(title: String, @ColorRes color: Int?) {
        rightIcon.visibility = View.GONE
        rightTv.setText(title)
        color?.let { rightTv.setTextColor(Utils.getApp().getColor(it)) }
    }

    override fun setRightIcon(@DrawableRes icon: Int) {
        rightTv.visibility = View.GONE
        rightIcon.setImageResource(icon)
    }


}