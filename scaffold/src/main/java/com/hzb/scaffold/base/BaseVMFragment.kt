package com.hzb.scaffold.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.hzb.scaffold.R
import com.hzb.scaffold.ScaffoldConfig
import com.hzb.scaffold.base.status.StatusType
import com.hzb.scaffold.base.status.StatusViewControl
import com.hzb.scaffold.base.toobar.ClickLocation
import com.hzb.scaffold.utils.ReflectUtil

abstract class BaseVMFragment<VM : BaseViewModel<BaseModel>, VB : ViewDataBinding> : Fragment() {
    //viewModel
    protected lateinit var viewModel: VM

    //布局持有
    protected lateinit var binding: VB

    //省缺页控制类
    private val statusControl by lazy {
        getStatusViewControl().apply {
            this.status.setRetryListener(::onRetryStatusEvent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createViewModel()
        initViewBefore()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = createDataBinding()
        initObserverEvent()
        initViewAfter()
        return view
    }

    /**
     * 布局初始化之前调用
     * @author zhibo.huang
     * @date 2024/5/9
     */
    open fun initViewBefore() {

    }

    open fun initObserverEvent() {
        viewModel.uiBaseEvent.backEvent.observe(viewLifecycleOwner) {
            (context as AppCompatActivity).finish()
        }
        viewModel.statusModel.statusSwitch.observe(viewLifecycleOwner) {
            statusControl.addStatusViewIfNeed()
            when (it.first) {
                StatusType.Empty -> {
                    statusControl.status.showEmpty(it.second)
                }

                StatusType.Error -> {
                    statusControl.status.showError(it.second)
                }

                StatusType.Loading -> {
                    statusControl.status.showLoading(it.second)
                }

                StatusType.Normal -> {
                    statusControl.removeStatusView()
                }
            }
        }
    }

    abstract fun initViewAfter()

    /**
     * 返回布局id
     *
     * @return
     */
    abstract fun getContentId(): Int

    /**
     * 返回databinding中绑定参数索引
     * @author zhibo.huang
     * @date 2024/5/9
     */
    abstract fun getViewModelBR(): Int

    /**
     * databinding相应初始化
     * @author zhibo.huang
     * @date 2024/5/9
     */
    private fun createDataBinding(): View {
        binding = DataBindingUtil.inflate<VB>(
            LayoutInflater.from(context),
            getContentId(),
            null,
            false
        )
        binding.setVariable(getViewModelBR(), viewModel)
        return binding.root
    }


    /**
     * 如果viewModel构造函数有需要额外传参,可以重新此方法,
     * 实现参考注释代码:
     * return object :ViewModelProvider.Factory{
     *             override fun <T : ViewModel> create(modelClass: Class<T>): T {
     *                return MainViewModel("id") as T
     *             }
     *         }
     * @author zhibo.huang
     * @date 2024/5/9
     */
    open fun createVMFactory(): ViewModelProvider.Factory? {
        return null
    }

    private fun createViewModel() {
        val clazzVM = ReflectUtil.getGenericityClass<VM>(this, 0)
        val vmFactory = createVMFactory()
        viewModel = if (vmFactory == null) {
            ViewModelProvider(this)[clazzVM]
        } else {
            ViewModelProvider(this, vmFactory)[clazzVM]
        }
        this.lifecycle.addObserver(viewModel)
    }


    protected fun getStatusViewControl(): StatusViewControl {
        return StatusViewControl(binding.root as ViewGroup, 0)
    }

    /**
     * 省缺页重试点击事件
     * @author zhibo.huang
     * @date 2024/5/16
     */
    open fun onRetryStatusEvent(view: View) {
        viewModel.statusModel.showLoading()
    }


    override fun onDestroy() {
        super.onDestroy()
        binding.unbind()
    }

}