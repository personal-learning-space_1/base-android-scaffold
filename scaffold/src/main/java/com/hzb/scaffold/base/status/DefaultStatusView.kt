package com.hzb.scaffold.base.status

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.isVisible
import com.blankj.utilcode.util.Utils
import com.hzb.scaffold.R
import com.hzb.scaffold.ScaffoldConfig

class DefaultStatusView : IStatusView() {

    lateinit var mTextInfo: TextView
    lateinit var mIcon: ImageView
    lateinit var mLoading: ProgressBar
    override fun onCreateView(context: Context): View {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_defalut_status, null)
        mTextInfo = view.findViewById(R.id.info)
        mLoading = view.findViewById(R.id.loadding)
        mIcon = view.findViewById(R.id.imageView)
        return view
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun showLoading(info: StatusInfo?) {
        mLoading.isVisible = true
        mIcon.isVisible = false
        val tempInfo = info ?: ScaffoldConfig.statusLoadingInfo
        tempInfo.icon?.let { mLoading.progressDrawable = Utils.getApp().getDrawable(tempInfo.icon) }
        mTextInfo.setText(tempInfo.info)
    }

    override fun showError(info: StatusInfo?) {
        mLoading.isVisible = false
        mIcon.isVisible = true
        val tempInfo = info ?: ScaffoldConfig.statusErrorInfo
        tempInfo.icon?.let { mIcon.setImageResource(it) }
        mTextInfo.setText(tempInfo.info)
        mTextInfo.setOnClickListener(listener)
    }

    override fun showEmpty(info: StatusInfo?) {
        mLoading.isVisible = false
        mIcon.isVisible = true
        val tempInfo = info ?: ScaffoldConfig.statusEmptyInfo
        tempInfo.icon?.let { mIcon.setImageResource(it) }
        mTextInfo.setText(tempInfo.info)
    }
}