package com.hzb.scaffold.base.status

import androidx.annotation.DrawableRes

data class StatusInfo(@DrawableRes val icon:Int?=null,val title:String?=null,val info:String?=null)
