package com.hzb.scaffold.base.status

import android.content.Context
import android.view.View
import androidx.annotation.DrawableRes

abstract class IStatusView {

    var listener: View.OnClickListener? = null
    abstract fun onCreateView(context: Context): View

    abstract fun showLoading(info: StatusInfo? = null)

    abstract fun showError(info: StatusInfo? = null)

    abstract fun showEmpty(info: StatusInfo? = null)

    fun setRetryListener(listener: View.OnClickListener) {
        this.listener = listener
    }
}