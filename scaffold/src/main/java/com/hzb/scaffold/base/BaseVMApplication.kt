package com.hzb.scaffold.base

import android.app.Application
import com.blankj.utilcode.util.CrashUtils
import com.blankj.utilcode.util.Utils
import com.hzb.scaffold.ScaffoldConfig
import com.hzb.scaffold.extension.logcate
import com.hzb.scaffold.utils.LogWrapUtil
import java.io.File


/**
 *
 * @author zhibo.huang
 * @date 2024/5/13
 */
abstract class BaseVMApplication : Application(),CrashUtils.OnCrashListener {

    private val CRASHDIR by lazy { "${this.externalCacheDir}${File.separator}crash" }
    private val LOGDIR by lazy { "${this.externalCacheDir}${File.separator}log" }

    override fun onCreate() {
        super.onCreate()
        instance = this
        appScaffoldConfig(ScaffoldConfig)
        Utils.init(this)
        CrashUtils.init(File(CRASHDIR).apply { mkdirs() },this)
        LogWrapUtil.init(LOGDIR)
    }

    abstract fun appScaffoldConfig(scaffoldConfig: ScaffoldConfig)


    abstract fun logUploadServer(dir:String)

    /**
     * 返回闪退日志的目录
     * @return:
     * @Author: zhibo.huang
     * @date:   2024/12/13
     */
    protected fun getCrashDir():String{
        return CRASHDIR
    }

    /**
     * 返回日志的目录
     * @return:
     * @Author: zhibo.huang
     * @date:   2024/12/13
     */
    protected fun getLogDir():String{
        return CRASHDIR
    }

    /**
     * 发送日志
     * @return:
     * @Author: zhibo.huang
     * @date:   2024/12/13
     */
    protected fun sendLog() {
        logUploadServer(LOGDIR)
    }

    override fun onCrash(crashInfo: CrashUtils.CrashInfo?) {
    }

    companion object{
        lateinit var instance:BaseVMApplication
    }
}