package com.hzb.scaffold.base

import android.app.Application
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import com.hzb.scaffold.base.paging.BasePagingViewModel
import com.hzb.scaffold.base.paging.PageKeyLoad
import com.hzb.scaffold.base.paging.PagingDataSource
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding

abstract class BaseListKeyViewModel<M : BaseModel, ITEM : Any>(application: Application) :
    BasePagingViewModel<M, ITEM>(
        application
    ) {


    abstract fun itemCallback(): DiffUtil.ItemCallback<ITEM>

    /**
     * Desc: RecyclerView的ItemBinding
     * <p>
     */
    abstract fun getItemBinding(): ItemBinding<ITEM>


    /**
     * 如果key存在null 说明没有更多数据
     * @author zhibo.huang
     * @date 2024/6/4
     */
    abstract fun loadData(key: String?, loadResult: PageKeyLoad.LoadResult<ITEM>)

    override fun createDataSource(): PagingDataSource<ITEM> {
        return object : PageKeyLoad<ITEM>(itemCallback(), { finishLoad(it) }) {
            override fun loadData(key: String?, loadResult: LoadResult<ITEM>) {
                this@BaseListKeyViewModel.loadData(key, loadResult)
            }

            override fun defalutFirstKey(): String {
                return firstIndex() as String
            }
        }
    }

    override fun firstIndex(): Any? {
        return ""
    }
}