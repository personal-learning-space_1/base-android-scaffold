package com.hzb.scaffold.base.paging

import com.hzb.scaffold.databinding.collections.DiffObservableArrayList

interface PagingDataSource<ITEM : Any> {

    /**
     * Desc: 加载数据
     * <p>
     * @param refresh 是否是刷新
     */
    fun loadOnlineData(refresh: Boolean)

    /**
     * Desc: 数据数组
     * <p>
     */
    fun getItems(): DiffObservableArrayList<ITEM>

    /**
     * Desc: 是否无更多数据
     * <p>
     */
    fun noMore(): Boolean

    /**
     * 获取已加载的页数
     */
    fun getLoadedPages(): Int

}