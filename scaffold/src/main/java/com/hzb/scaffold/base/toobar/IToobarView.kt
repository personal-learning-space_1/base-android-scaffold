package com.hzb.scaffold.base.toobar

import android.content.Context
import android.view.View
import android.view.View.OnClickListener
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes

/**
 * toobar 接口约束
 * @author zhibo.huang
 * @date 2024/5/13
 */
abstract class IToobarView {


    var callback :((location:ClickLocation)->Unit)?=null

    /**
     * @Description:
     * @return:
     * @author:  zhibo.huang
     * @date:  2024/5/13
     */
    abstract fun createView(context: Context): View

    abstract fun setTitleText(title:String,@ColorRes color:Int?=null)

    abstract fun setLeftIcon(@DrawableRes icon:Int)

    abstract fun setRightText(title:String,@ColorRes color:Int?=null)

    abstract fun setRightIcon(@DrawableRes icon:Int)

    fun clickCommand(callback:(location:ClickLocation)->Unit){
        this.callback =callback
    }

}

sealed class ClickLocation{
    data object Left:ClickLocation()
    data object Right:ClickLocation()
}