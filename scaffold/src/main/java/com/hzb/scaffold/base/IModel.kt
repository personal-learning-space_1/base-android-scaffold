package com.hzb.scaffold.base

interface IModel {

    fun onCleared()
}