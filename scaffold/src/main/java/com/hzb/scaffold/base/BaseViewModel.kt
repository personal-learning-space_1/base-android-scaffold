package com.hzb.scaffold.base

import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hzb.scaffold.base.status.StatusViewModel
import com.hzb.scaffold.databinding.bindAction
import com.hzb.scaffold.utils.ReflectUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

open class BaseViewModel<M : BaseModel>(application: Application) : AndroidViewModel(application),
    DefaultLifecycleObserver {

    //初始化model:默认无参数初始化
    protected val model: M by lazy {
        try {
            initModel() ?: ReflectUtil.getGenericity2Instance<M>(this, 0)
        } catch (e: Exception) {
            BaseModel() as M
        }
    }

    //状态页控制
    val statusModel: StatusViewModel by lazy { StatusViewModel() }


    /**
     * @Description: 如果需要手动初始化model,可以实现这个方法
     * @author:  zhibo.huang
     * @date:  2024/4/28
     */
    protected fun initModel(): M? {
        return null
    }

    // UI事件
    val uiBaseEvent by lazy { UIBaseLiveEvent() }


    inner class UIBaseLiveEvent {
        //返回
        val backEvent = MutableLiveData<Any>()
    }

    fun finish() {
        uiBaseEvent.backEvent.postValue(true)
    }

    val finish = bindAction {
        finish()
    }

    override fun onCleared() {
        model.onCleared()
        super.onCleared()
    }

    open fun onActivityResult(requestCode: Int, data: Intent?) {}
}