package com.hzb.scaffold.base.paging

import androidx.recyclerview.widget.DiffUtil
import com.hzb.scaffold.databinding.collections.DiffObservableArrayList
import java.util.concurrent.atomic.AtomicInteger

/**
 * 页数分页的加载功能
 * @author zhibo.huang
 * @date 2024/6/4
 */
abstract class PageIndexLoad<ITEM : Any>(
    val callback: DiffUtil.ItemCallback<ITEM>,
    val loadResult: (error: Boolean) -> Unit
) : PagingDataSource<ITEM> {

    /**
     * 数据源
     */
    private val source = DiffObservableArrayList(callback)

    val nextLoadIndex = AtomicInteger()

    /**
     * 总页数
     */
    private var totalPages = 10

    override fun loadOnlineData(refresh: Boolean) {
        if (refresh) {
            loadData(defalutIndex(),LoadResultImpl)
        }else{
            loadData(nextLoadIndex.get(),LoadResultImpl)
        }
    }

    override fun getItems() = source

    override fun noMore(): Boolean {
        return nextLoadIndex.get() > totalPages
    }

    override fun getLoadedPages(): Int {
        return nextLoadIndex.get() - defalutIndex()
    }


    abstract fun loadData(index:Int,loadResult:LoadResult<ITEM>)


    protected open fun  defalutIndex() = 0


    private val LoadResultImpl by lazy {
        object : LoadResult<ITEM> {
            override fun success(data: List<ITEM>, index: Int, total: Int) {
                this@PageIndexLoad.nextLoadIndex.set(index+1)
                this@PageIndexLoad.totalPages = total
                this@PageIndexLoad.getItems().submit(data, index>defalutIndex())
                this@PageIndexLoad.loadResult(false)
            }

            override fun faile(exception: Exception) {
                this@PageIndexLoad.loadResult(true)
            }

        }
    }

    interface LoadResult<ITEM> {
         fun success(data: List<ITEM>, index: Int, total: Int)

         fun faile(exception: Exception)
    }
}