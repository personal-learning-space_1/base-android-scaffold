package com.hzb.scaffold.base.paging

import androidx.recyclerview.widget.DiffUtil
import com.hzb.scaffold.databinding.collections.DiffObservableArrayList

/**
 * key 分页加载功能
 * @author zhibo.huang
 * @date 2024/6/4
 */
abstract class PageKeyLoad<ITEM : Any>(
    val callback: DiffUtil.ItemCallback<ITEM>,
    val loadResult: (error: Boolean) -> Unit
) : PagingDataSource<ITEM> {

    /**
     * 数据源
     */
    private val source = DiffObservableArrayList(callback)

    var nextLoadIndex: String? = ""

    /**
     * 总页数
     */
    private var totalPages = 10

    override fun loadOnlineData(refresh: Boolean) {
        if (refresh) {
            loadData(defalutFirstKey(), LoadResultImpl)
        } else {
            loadData(nextLoadIndex, LoadResultImpl)
        }
    }

    override fun getItems() = source

    override fun noMore(): Boolean {
        return nextLoadIndex == null
    }

    override fun getLoadedPages(): Int {
        return totalPages
    }


    /**
     * 如果key存在null 说明没有更多数据
     * @author zhibo.huang
     * @date 2024/6/4
     */
    abstract fun loadData(key: String?, loadResult: LoadResult<ITEM>)


    protected open fun defalutFirstKey(): String = ""


    private val LoadResultImpl by lazy {
        object : LoadResult<ITEM> {
            override fun success(
                data: List<ITEM>,
                currentKey: String,
                nextKey: String,
                total: Int
            ) {
                this@PageKeyLoad.nextLoadIndex = nextKey
                this@PageKeyLoad.totalPages = total
                this@PageKeyLoad.getItems().submit(data, currentKey != defalutFirstKey())
                this@PageKeyLoad.loadResult(false)
            }

            override fun faile(exception: Exception) {
                this@PageKeyLoad.loadResult(true)
            }

        }
    }

    interface LoadResult<ITEM> {
        fun success(data: List<ITEM>, currentKey: String, nextKey: String, total: Int)

        fun faile(exception: Exception)
    }
}