package com.hzb.scaffold.base.status

import androidx.lifecycle.MutableLiveData
import com.hzb.scaffold.base.BaseModel
import com.hzb.scaffold.base.BaseViewModel

/**
 * 状态省缺页 viewModel
 * @author zhibo.huang
 * @date 2024/5/16
 */
class StatusViewModel {

    val statusSwitch = MutableLiveData<Pair<StatusType, StatusInfo?>>()


    fun showLoading(info: StatusInfo?=null) {
        statusSwitch.postValue(StatusType.Loading to info)
    }

    fun showError(info: StatusInfo?=null) {
        statusSwitch.postValue(StatusType.Error to info)
    }

    fun showNormal() {
        statusSwitch.postValue(StatusType.Normal to null)
    }

    fun showEmpty(info: StatusInfo?=null) {
        statusSwitch.postValue(StatusType.Empty to info)
    }

}

sealed class StatusType {
    data object Error : StatusType()
    data object Loading : StatusType()
    data object Normal : StatusType()
    data object Empty : StatusType()
}