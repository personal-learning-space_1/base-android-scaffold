package com.hzb.scaffold.view.linearLayout

import android.view.View
import android.widget.LinearLayout

/**
 * LinearLayout适配器模式
 * @author zhibo.huang
 * @date 2024/6/3
 */
internal interface LinearLayoutAdapter {

    fun onCreateView(linearLayout: LinearLayout, position: Int): View

    fun onViewBinding(itemView: View, position: Int)

    fun getItemCount(): Int

}

fun LinearLayout.setAdapter(linearLayoutAdapter: BaseLinearLayoutAdapter) {
    tag = linearLayoutAdapter
    linearLayoutAdapter.injectLinearLayout(this)
    removeAllViews()
    val itemCount = linearLayoutAdapter.getItemCount()
    for (position in 0 until itemCount) {
        val itemView = linearLayoutAdapter.onCreateView(this, position)
        addView(itemView)
        linearLayoutAdapter.onViewBinding(itemView, position)
    }
}

fun LinearLayout.getAdapter() = this.tag as? BaseLinearLayoutAdapter

/**
 * Desc: 刷新整个列表
 * <p>
 * author: linjiaqiang
 * Date: 2019/10/15
 */
fun LinearLayout.notifyDataSetChanged() {
    val adapter = getAdapter()
    adapter?.apply { setAdapter(adapter) }
}