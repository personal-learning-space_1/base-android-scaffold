package com.hzb.scaffold.view.recyclerview;

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.hzb.scaffold.permission.VoidCallback
import java.util.Arrays

/**
 *
 * @author zhibo.huang
 * @date   2024/8/2
 */
internal class PreLoadScrollListener(private val onLoadMoreListener: VoidCallback, private val preLoadOffset: Int=3) : RecyclerView.OnScrollListener() {

    /**
     * 存储瀑布流item坐标
     */
    private var staggeredColumnsPositions: IntArray? = null

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (dy <= 0) {
            return
        }
        val adapter = recyclerView.adapter as? BindingRecyclerViewAdapter<*>
        if (null != adapter) {
            val dataCount = 0.coerceAtLeast(adapter.itemCount)
            if (dataCount == 0) {
                return
            }
            val layoutManager = recyclerView.layoutManager
            if (null == layoutManager) {
                return
            }
            val lastVisiblePosition = getLastVisiblePosition(recyclerView)
            val visibleItemCount = layoutManager.childCount
            if (visibleItemCount > lastVisiblePosition) {
                return
            }
            val preLoadPosition = adapter.itemCount - preLoadOffset
            if (lastVisiblePosition >= preLoadPosition) {
                onLoadMoreListener.invoke()
            }
        }
    }

    /**
     * Desc: 获取最后一个可见item的position
     * <p>
     * author: linjiaqiang
     * Date: 2019/12/4
     */
    private fun getLastVisiblePosition(recyclerView: RecyclerView): Int {
        val layoutManager = recyclerView.layoutManager
        var lastVisiblePosition = 0
        try {
            if (layoutManager is LinearLayoutManager) {
                lastVisiblePosition = layoutManager.findLastVisibleItemPosition()
            } else if (layoutManager is StaggeredGridLayoutManager) {
                val spansCount = layoutManager.spanCount
                if (spansCount == 0) {
                    return lastVisiblePosition
                }
                if (null == staggeredColumnsPositions) {
                    staggeredColumnsPositions = IntArray(spansCount)
                }
                val result = layoutManager.findLastVisibleItemPositions(staggeredColumnsPositions)
                Arrays.sort(result)
                lastVisiblePosition = result[result.size - 1]
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return lastVisiblePosition
    }



    companion object {
        private const val DEBUG = false
        private const val TAG = "PreLoadScrollListener"
    }
}