package com.hzb.scaffold.view.linearLayout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import com.hzb.scaffold.view.recyclerview.BindingCollectionAdapter
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding
import java.lang.ref.WeakReference

/**
 * LinearLayout适配器模式DataBinding支持
 * @author zhibo.huang
 * @date 2024/6/3
 */
class BindingLinearLayoutAdapter<T> : BaseLinearLayoutAdapter(), BindingCollectionAdapter<T> {

    private var itemBinding: ItemBinding<in T>? = null
    private var items: List<T>? = null
    private var inflater: LayoutInflater? = null
    private var callback: WeakReferenceOnListChangedCallback<T>? = null

    override fun onCreateView(linearLayout: LinearLayout, position: Int): View {
        requireNotNull(itemBinding) { "itemBinding == null" }
        requireNotNull(items) { "items == null" }
        itemBinding!!.onItemBind(position, items!![position])
        if (inflater == null) {
            inflater = LayoutInflater.from(linearLayout.context)
        }
        val binding = onCreateBinding(inflater!!, itemBinding!!.layoutRes(), linearLayout)
        return binding.root
    }

    override fun onViewBinding(itemView: View, position: Int) {
        val binding = DataBindingUtil.getBinding<ViewDataBinding>(itemView)
        val item = items!![position]
        onBindBinding(binding!!, itemBinding!!.variableId(), itemBinding!!.layoutRes(), position, item)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun setItemBinding(itemBinding: ItemBinding<in T>) {
        this.itemBinding = itemBinding
    }

    override fun onCreateBinding(inflater: LayoutInflater, layoutRes: Int, viewGroup: ViewGroup): ViewDataBinding {
        return DataBindingUtil.inflate(inflater, layoutRes, viewGroup, false)
    }

    override fun getItemBinding(): ItemBinding<in T> {
        requireNotNull(itemBinding) { "itemBinding == null" }
        return itemBinding!!
    }

    override fun setItems(items: List<T>?) {
        if (this.items === items) {
            return
        }
        if (this.items is ObservableList<T>) {
            (this.items as ObservableList<T>).removeOnListChangedCallback(callback)
            callback = null
        }
        if (items is ObservableList<T>) {
            callback = WeakReferenceOnListChangedCallback(this)
            items.addOnListChangedCallback(callback)
        }
        this.items = items
        notifyDataSetChanged()
    }

    override fun getAdapterItem(position: Int): T {
        return items!![position]
    }

    override fun onBindBinding(binding: ViewDataBinding, variableId: Int, layoutRes: Int, position: Int, item: T) {
        val bound = itemBinding!!.bind(binding, item)
        if (bound) {
            binding.executePendingBindings()
        }
    }

    internal class WeakReferenceOnListChangedCallback<T>(adapter: BindingLinearLayoutAdapter<T>) : ObservableList.OnListChangedCallback<ObservableList<T>>() {

        private val adapterRef: WeakReference<BindingLinearLayoutAdapter<T>> = WeakReference(adapter)

        override fun onChanged(sender: ObservableList<T>?) {
            val adapter = adapterRef.get() ?: return
            adapter.notifyDataSetChanged()
        }

        override fun onItemRangeChanged(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
            onChanged(sender)
        }

        override fun onItemRangeInserted(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
            onChanged(sender)
        }

        override fun onItemRangeMoved(sender: ObservableList<T>?, fromPosition: Int, toPosition: Int, itemCount: Int) {
            onChanged(sender)
        }

        override fun onItemRangeRemoved(sender: ObservableList<T>?, positionStart: Int, itemCount: Int) {
            onChanged(sender)
        }
    }

}