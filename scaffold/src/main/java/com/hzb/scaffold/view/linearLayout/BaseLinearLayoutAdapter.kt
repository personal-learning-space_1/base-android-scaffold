package com.hzb.scaffold.view.linearLayout

import android.widget.LinearLayout

/**
 * LinearLayout适配器模式Adapter
 * @author zhibo.huang
 * @date 2024/6/3
 */
abstract class BaseLinearLayoutAdapter : LinearLayoutAdapter {

    private var linearLayout: LinearLayout? = null

    internal fun injectLinearLayout(linearLayout: LinearLayout) {
        this.linearLayout = linearLayout
    }

    /**
     * Desc: 刷新整个列表
     * <p>
     * author: linjiaqiang
     * Date: 2019/10/15
     */
    fun notifyDataSetChanged() {
        linearLayout?.notifyDataSetChanged()
    }
}