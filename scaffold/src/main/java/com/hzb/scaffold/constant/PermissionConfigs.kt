package com.hzb.scaffold.constant

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.Manifest.permission.READ_MEDIA_AUDIO
import android.Manifest.permission.READ_MEDIA_IMAGES
import android.Manifest.permission.READ_MEDIA_VIDEO
import android.Manifest.permission.READ_MEDIA_VISUAL_USER_SELECTED
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.os.Build
import com.blankj.utilcode.util.DeviceUtils


object PermissionConfigs {

    /**
     * 获取图片权限字段 适配到34版本
     * @author zhibo.huang
     * @date   2024/11/6
     */
    fun getImagePermission(context: Context): Array<String> {
        val targetSdkVersion = context.applicationInfo.targetSdkVersion
        if (DeviceUtils.getSDKVersionCode() >= 34) {
            return if (targetSdkVersion >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                arrayOf<String>(READ_MEDIA_VISUAL_USER_SELECTED, READ_MEDIA_IMAGES)
            } else if (targetSdkVersion == Build.VERSION_CODES.TIRAMISU) {
                arrayOf<String>(READ_MEDIA_IMAGES)
            } else {
                arrayOf<String>(READ_EXTERNAL_STORAGE)
            }
        } else if (DeviceUtils.getSDKVersionCode() >= 33) {
            return if (targetSdkVersion >= Build.VERSION_CODES.TIRAMISU)
                arrayOf(READ_MEDIA_IMAGES)
            else arrayOf(READ_EXTERNAL_STORAGE)
        }else{
            return arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
        }
    }


    /**
     * 获取视频权限字段 适配到34版本
     * @author zhibo.huang
     * @date   2024/11/6
     */
    fun getVideoPermission(context: Context): Array<String> {
        val targetSdkVersion = context.applicationInfo.targetSdkVersion
        if (DeviceUtils.getSDKVersionCode() >= 34) {
            return if (targetSdkVersion >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                arrayOf<String>(READ_MEDIA_VISUAL_USER_SELECTED, READ_MEDIA_VIDEO)
            } else if (targetSdkVersion == Build.VERSION_CODES.TIRAMISU) {
                arrayOf<String>(READ_MEDIA_VIDEO)
            } else {
                arrayOf<String>(READ_EXTERNAL_STORAGE)
            }
        } else if (DeviceUtils.getSDKVersionCode() >= 33) {
            return if (targetSdkVersion >= Build.VERSION_CODES.TIRAMISU)
                arrayOf(READ_MEDIA_VIDEO)
            else arrayOf(READ_EXTERNAL_STORAGE)
        }else{
            return arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
        }
    }

    /**
     * 获取语音权限字段 适配到34版本
     * @author zhibo.huang
     * @date   2024/11/6
     */
    fun getAudioPermission(context: Context): Array<String> {
        val targetSdkVersion = context.applicationInfo.targetSdkVersion
        if (DeviceUtils.getSDKVersionCode() >= 34) {
            return if (targetSdkVersion >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                arrayOf<String>(READ_MEDIA_VISUAL_USER_SELECTED, READ_MEDIA_AUDIO)
            } else if (targetSdkVersion == Build.VERSION_CODES.TIRAMISU) {
                arrayOf<String>(READ_MEDIA_AUDIO)
            } else {
                arrayOf<String>(READ_EXTERNAL_STORAGE)
            }
        } else if (DeviceUtils.getSDKVersionCode() >= 33) {
            return if (targetSdkVersion >= Build.VERSION_CODES.TIRAMISU)
                arrayOf(READ_MEDIA_AUDIO)
            else arrayOf(READ_EXTERNAL_STORAGE)
        }else{
            return arrayOf(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE)
        }
    }
}