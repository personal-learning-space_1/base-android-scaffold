package com.hzb.scaffold.constant

object HttpCode {
    //其他错误
    val OTHER_ERR = -1

    //网络错误
    val HTTP_NETWORK_ERROR = -2

}