package com.hzb.scaffold.utils

import java.lang.reflect.ParameterizedType

/**
 * @Description:java 反射相关方法
 * @author zhibo.huang
 * @date 2024/4/28
 */
object ReflectUtil {


    /**
     * @Description:泛型实例化
     * @author zhibo.huang
     * @date 2024/5/9
     */
    fun <T> getGenericity2Instance(objects: Any, index:Int):T{
        val clazz =getGenericityClass<T>(objects,index)
        return clazz.newInstance() as T
    }

    /**
     * @Description:获取对象中,指定的泛型class
     * @author zhibo.huang
     * @date 2024/5/9
     */
    fun <T> getGenericityClass(objects: Any, index:Int):Class<T>{
        val type =  if (objects is Class<*>){
            objects.genericSuperclass
        }else {
            objects.javaClass.genericSuperclass
        }
        check(type is ParameterizedType){
            "objects 参数中不存在泛型"
        }
       return type.actualTypeArguments[index] as Class<T>
    }
}