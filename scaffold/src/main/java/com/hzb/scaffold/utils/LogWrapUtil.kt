package com.hzb.scaffold.utils

import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.PathUtils
import com.hzb.scaffold.ScaffoldConfig
import java.io.File

object LogWrapUtil {

    fun init(filePath: String = PathUtils.getExternalAppCachePath() + File.separator + "log") {
        LogUtils.getConfig().apply {
            stackOffset = 1
            isLog2FileSwitch=!ScaffoldConfig.isDebug
            dir = filePath
            saveDays = 7
        }
    }


    fun logI(tag:String,info:Any){
        LogUtils.iTag("${tag}", info)
    }

    fun logE(tag:String,info:Any){
        LogUtils.eTag("${tag}", info)
    }

    fun logW(tag:String,info:Any){
        LogUtils.wTag("${tag}", info)
    }

}