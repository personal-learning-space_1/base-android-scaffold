package com.hzb.scaffold.extension

import android.os.Looper
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.hzb.scaffold.base.BaseViewModel
import com.hzb.scaffold.constant.HttpCode
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import kotlin.coroutines.CoroutineContext

/**
 * @Description: main线程下的协程
 * @author:  zhibo.huang
 * @date:  2024/4/28
 */
fun BaseViewModel<*>.launch(
    black: suspend CoroutineScope.() -> Unit,
    fail: ((code: Int, err: Exception) -> Unit)? = null,
    showErrToast: Boolean = true
) {
    launch(Dispatchers.Main, black, fail, showErrToast = showErrToast)
}

/**
 * @Description: IO线程下的协程
 * @author:  zhibo.huang
 * @date:  2024/4/28
 */
fun BaseViewModel<*>.launchIO(
    black: suspend kotlinx.coroutines.CoroutineScope.() -> Unit,
    fail: ((code: Int, err: Exception) -> Unit)? = null
) {
    launch(Dispatchers.IO, black, fail, false)
}

/**
 * @Description: 子线程下的协程
 * @author:  zhibo.huang
 * @date:  2024/4/28
 */
fun BaseViewModel<*>.launchThread(
    black: suspend kotlinx.coroutines.CoroutineScope.() -> Unit,
    fail: ((code: Int, err: Exception) -> Unit)? = null
) {
    launch(Dispatchers.Default, black, fail, false)
}


/**
 * viewModel 协程扩展
 * @author:  zhibo.huang
 * @date:  2024/4/28
 */
private fun BaseViewModel<*>.launch(
    context: CoroutineContext,
    black: suspend kotlinx.coroutines.CoroutineScope.() -> Unit,
    fail: ((code: Int, err: Exception) -> Unit)? = null,
    showErrToast: Boolean = true
) {
    val handler = CoroutineExceptionHandler { _, throwable ->
        when (throwable) {
            is HttpException -> {
                fail?.invoke(throwable.code(), throwable)
            }

            is Exception -> {
                fail?.invoke(HttpCode.OTHER_ERR, throwable)
            }
        }
        if (showErrToast && Looper.getMainLooper().thread == Thread.currentThread()) {
            toastLong { throwable.message.toString() }
        }
    }

    viewModelScope.launch(context + handler) {
        black()
    }
}
