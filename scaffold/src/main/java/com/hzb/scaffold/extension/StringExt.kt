package com.hzb.scaffold.extension

import android.graphics.Color

/**
 * 颜色
 * @return:
 * @Author: zhibo.huang
 * @date:   2024/8/13
 */
fun String.parseColor(): Int {
    return Color.parseColor(this)
}