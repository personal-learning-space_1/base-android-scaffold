package com.hzb.scaffold.extension

import com.blankj.utilcode.util.SizeUtils

fun Float.dp():Int{
    return SizeUtils.dp2px(this)
}

fun Float.sp():Int{
    return SizeUtils.sp2px(this)
}