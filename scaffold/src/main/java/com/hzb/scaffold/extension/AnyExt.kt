package com.hzb.scaffold.extension

import android.widget.Toast
import com.blankj.utilcode.util.ActivityUtils
import com.google.gson.Gson
import com.hzb.scaffold.utils.LogWrapUtil

inline fun <reified T : Any> T.logcatw(
    prefix:String?="log_",
    tag: String = T::class.java.simpleName,
    crossinline block: () -> Any
) = LogWrapUtil.logW("${prefix}${tag}",block())

inline fun <reified T : Any> T.logcate(
    prefix:String?="log_",
    tag: String = T::class.java.simpleName,
    crossinline block: () -> Any
) = LogWrapUtil.logE("${prefix}${tag}",block())

inline fun <reified T : Any> T.logcati(
    prefix:String?="log_",
    tag: String = T::class.java.simpleName,
    crossinline block: () -> Any
) = LogWrapUtil.logI("${prefix}${tag}",block())

inline fun <reified T : Any> T.toastShort(
    crossinline block: () -> String
) = Toast.makeText(ActivityUtils.getTopActivity(),block(),Toast.LENGTH_SHORT).show()

inline fun <reified T : Any> T.toastLong(
    crossinline block: () -> String
) = Toast.makeText(ActivityUtils.getTopActivity(),block(),Toast.LENGTH_LONG).show()


fun Any?.toGson():String{
    return Gson().toJson(this).toString()
}