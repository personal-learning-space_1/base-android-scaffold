package com.hzb.scaffold.extension

import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.hzb.scaffold.base.BaseVMApplication

fun Int.parseColor(): Int {
    return ContextCompat.getColor(BaseVMApplication.instance, this)
}

fun Int.parseString(vararg arg: Any?= arrayOf()): String {
    return BaseVMApplication.instance.getString(this, *arg)
}

fun Int.parseDrawable(): Drawable? {
    return ContextCompat.getDrawable(BaseVMApplication.instance, this)
}