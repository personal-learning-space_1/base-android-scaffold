package com.hzb.scaffold.net

import com.blankj.utilcode.util.LogUtils
import com.hzb.scaffold.ScaffoldConfig
import com.hzb.scaffold.base.BaseVMApplication
import com.hzb.scaffold.extension.logcati
import com.hzb.scaffold.net.interceptor.BaseUrlInterceptor
import com.hzb.scaffold.net.interceptor.CacheInterceptor
import com.hzb.scaffold.net.interceptor.LogInterceptor
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import java.io.File
import java.util.concurrent.TimeUnit


/**
 *
 * @author zhibo.huang
 * @date 2024/5/25
 */
open class DefaultHttpClient : IHttpClient {

    private val httpLoggingInterceptor by lazy {
        LogInterceptor(object : LogInterceptor.Logger {
            override fun log(message: String) {
                logcati("okhttp") { message }
            }
        }).apply {
            this.level = LogInterceptor.Level.BODY
        }
    }

    override fun getInterceptors(): Array<Interceptor> {
        return arrayOf()
    }

    override fun getTimeout(): Long {
        return ScaffoldConfig.timeout
    }

    override fun getBaseUrl(): String {
        return ScaffoldConfig.hostUrl
    }



    override fun getCache(): Cache {
        val httpCacheDirectory: File = File(BaseVMApplication.instance.cacheDir, "http-cache")
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(httpCacheDirectory, cacheSize.toLong())
    }

    override fun openLogPrint(): Boolean {
        return LogUtils.getConfig().isLogSwitch
    }

    override fun build(): OkHttpClient {
        return OkHttpClient.Builder().apply {
            addInterceptor(BaseUrlInterceptor())
            addInterceptor(CacheInterceptor())
            if (openLogPrint()) {
                addInterceptor(httpLoggingInterceptor)
            }
            getInterceptors().forEach {
                addInterceptor(it)
            }
            this.connectTimeout(getTimeout(), TimeUnit.MILLISECONDS)
            this.cache(getCache())
        }.build()

    }
}