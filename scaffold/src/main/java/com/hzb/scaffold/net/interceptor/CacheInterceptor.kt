package com.hzb.scaffold.net.interceptor

import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Interceptor.*
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * 缓存控制拦截器
 * @author zhibo.huang
 * @date 2024/5/25
 */
class CacheInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val response: Response = chain.proceed(chain.request())

        val cacheControl: CacheControl = CacheControl.Builder()
            .maxAge(1, TimeUnit.MINUTES) // 1 minutes cache
            .build()

        return response.newBuilder()
            .removeHeader("Pragma")
            .removeHeader("Cache-Control")
            .header("Cache-Control", cacheControl.toString())
            .build()
    }
}