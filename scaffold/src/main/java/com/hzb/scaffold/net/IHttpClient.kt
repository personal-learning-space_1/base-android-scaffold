package com.hzb.scaffold.net

import okhttp3.Authenticator
import okhttp3.Cache
import okhttp3.CookieJar
import okhttp3.Interceptor

interface IHttpClient {

    fun getInterceptors():Array<Interceptor>

    fun getTimeout():Long

    fun getBaseUrl():String

    fun getCache():Cache

    fun openLogPrint():Boolean


    fun  build():okhttp3.OkHttpClient
}