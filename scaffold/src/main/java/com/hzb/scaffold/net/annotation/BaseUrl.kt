package com.hzb.scaffold.net.annotation

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class BaseUrl(val value: String = "")
