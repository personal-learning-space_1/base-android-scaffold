package com.hzb.scaffold.net.interceptor

import com.hzb.scaffold.net.annotation.BaseUrl
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Interceptor.*
import okhttp3.Response
import retrofit2.Invocation
import java.io.IOException

/**
 * BaseUrl拦截器
 * @author zhibo.huang
 * @date 2024/5/25
 */
class BaseUrlInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val invocation = chain.request().tag(Invocation::class.java)
        invocation?.apply {
            val baseUrl = this.method().getAnnotation(BaseUrl::class.java)?.value!!
            val newUrl = HttpUrl.parse(baseUrl)
            val originUrl = chain.request().url()
            val newHttpUrl = originUrl.newBuilder().scheme(newUrl!!.scheme())
                .host(newUrl.host())
                .port(newUrl.port())
                .build()
            val newRequest = chain.request().newBuilder().url(newHttpUrl).build()
            return chain.proceed(newRequest)
        }
        return chain.proceed(chain.request())
    }
}