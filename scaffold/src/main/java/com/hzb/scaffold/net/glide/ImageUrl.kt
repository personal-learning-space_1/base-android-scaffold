package com.hzb.scaffold.net.glide

import com.bumptech.glide.load.model.GlideUrl

class ImageUrl(val key: String? = null, val url: String? = null) : GlideUrl(url) {

    override fun getCacheKey(): String {
        return if (key.isNullOrBlank()) {
            super.getCacheKey()
        } else {
            key
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        if (!super.equals(other)) return false

        other as ImageUrl

        if (key != other.key) return false
        if (url != other.url) return false

        return true
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + (key?.hashCode() ?: 0)
        result = 31 * result + (url?.hashCode() ?: 0)
        return result
    }


}