package com.hzb.scaffold.net

import com.hzb.scaffold.ScaffoldConfig
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Retrofit

/**
 * @description 网络请求入口类
 * @author zhibo.huang
 * @date 2024/5/25
 */
class RetrofitManager private constructor() {


    private val retrofitClient: Retrofit by lazy {
        Retrofit.Builder().baseUrl(ScaffoldConfig.hostUrl)
            .client(ScaffoldConfig.okHttpClient.build())
            .addConverterFactory(ScaffoldConfig.responseParser)
            .build()
    }


    /**
     *
     * @author zhibo.huang
     * @date 2024/5/25
     */
    fun <T> create(service: Class<T>?): T {
        if (service == null) {
            throw RuntimeException("Api service is null!")
        }
        return retrofitClient.create<T>(service)
    }


    companion object {
        @JvmStatic
        val instance by lazy { RetrofitManager() }
    }

}