package com.hzb.scaffold.permission

import android.Manifest
import android.content.Intent
import com.hzb.scaffold.base.BaseVMApplication

/**
 * 权限申请入口类
 * @author zhibo.huang
 * @date 2024/5/21
 */
object PermissionLauncher {
    //授权成功回调
    var grantCallback: VoidCallback? = null
    //拒绝回调
    var deniedCallback: VoidCallback? = null

    fun requestPermission(
        permiss: Array<String>,
        description: String? = null,
        grant: VoidCallback?=null,
        denied:VoidCallback?=null,
    ) {
        this.grantCallback = grant
        this.deniedCallback = denied
        PermissionDescActivity.launch(BaseVMApplication.instance,permiss,description)
    }
}
typealias VoidCallback = () -> Unit