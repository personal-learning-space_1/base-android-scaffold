package com.hzb.scaffold.permission

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isVisible
import androidx.core.view.postDelayed
import androidx.lifecycle.lifecycleScope
import com.blankj.utilcode.util.PermissionUtils
import com.hzb.scaffold.R
import com.hzb.scaffold.base.BaseVMApplication
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class PermissionDescActivity : AppCompatActivity() {

    val permiss by lazy { intent.getStringArrayExtra("permissions") }
    val desc by lazy { intent.getStringExtra("desc") }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        handleTransition()
        setContentView(R.layout.activity_permission_desc)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        findViewById<TextView>(R.id.info).text = desc
        this.lifecycleScope.launch {
            delay(300)
            findViewById<View>(R.id.main).isVisible = true
        }

        PermissionUtils.permission(*permiss)
            .callback(object : PermissionUtils.FullCallback {
                override fun onGranted(p0: MutableList<String>) {
                    PermissionLauncher.grantCallback?.invoke()
                    this@PermissionDescActivity.finish()
                }

                override fun onDenied(p0: MutableList<String>, p1: MutableList<String>) {
                    PermissionLauncher.deniedCallback?.invoke()
                    this@PermissionDescActivity.finish()
                }

            }).request()
    }


    private fun handleTransition() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            overrideActivityTransition(OVERRIDE_TRANSITION_OPEN, -1, -1, Color.TRANSPARENT)
            overrideActivityTransition(OVERRIDE_TRANSITION_CLOSE, -1, -1, Color.TRANSPARENT)
        } else {
            overridePendingTransition(-1, -1)
        }
    }

    companion object {
        fun launch(context: Context, permiss: Array<String>, desc: String?) {
            val intent = Intent(context, PermissionDescActivity::class.java)
            intent.putExtra("permissions", permiss)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra("desc", desc)
            context.startActivity(intent)
        }
    }
}