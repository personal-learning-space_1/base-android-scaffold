package com.hzb.scaffold

import com.hzb.scaffold.base.status.DefaultStatusView
import com.hzb.scaffold.base.status.IStatusView
import com.hzb.scaffold.base.status.StatusInfo
import com.hzb.scaffold.base.toobar.DefalutToobarView
import com.hzb.scaffold.base.toobar.IToobarView
import com.hzb.scaffold.net.DefaultHttpClient
import com.hzb.scaffold.net.IHttpClient
import retrofit2.converter.gson.GsonConverterFactory

/**
 * @Description:默认参数配置，涉及到网络host,日志开启等配置
 * @author zhibo.huang
 * @date 2024/4/24
 */
object ScaffoldConfig {

    //配置toobar样式
    var toobarView: IToobarView = DefalutToobarView()

    //配置省缺页样式
    var statusView: IStatusView = DefaultStatusView()
    //错误状态页信息配置
    var statusErrorInfo = StatusInfo(title = "出错了")
    //加载中状态页信息配置
    var statusLoadingInfo = StatusInfo(title = "加载中")
    //空数据状态页信息配置
    var statusEmptyInfo = StatusInfo(title = "空数据")

    //网络host配置
    var hostUrl = "https://www.wanandroid.com"
    //网络请求超时
    var timeout:Long=60*1000
    //核心请求网络实现
    var okHttpClient:IHttpClient = DefaultHttpClient()
    //response 解析器
    var responseParser = GsonConverterFactory.create()

    //是否debug 模式（日志打印）
    var isDebug = true

}