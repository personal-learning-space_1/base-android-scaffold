package com.hzb.scaffold.databinding

import android.widget.CheckBox
import androidx.databinding.BindingAdapter


/**
 * 选中监听
 * @return: 
 * @author:  zhibo.huang
 * @date:  2024/5/27
 */
@BindingAdapter(value = ["onCheckedChangedCommand"], requireAll = false)
fun setCheckedChanged(checkBox: CheckBox, bindingCommand: BindCommandConsumer<Boolean>) {
    checkBox.setOnCheckedChangeListener { _, b -> bindingCommand.invoke(b) }
}