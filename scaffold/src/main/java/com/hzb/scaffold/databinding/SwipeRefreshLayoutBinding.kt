package com.hzb.scaffold.databinding

import android.annotation.SuppressLint
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.hzb.scaffold.base.paging.RefreshStatus


//下拉刷新命令
@BindingAdapter("onRefreshCommand", "refreshingChanged", requireAll = false)
fun SwipeRefreshLayout.onRefreshCommand(
    onRefreshCommand: BindCommand?,
    refreshingChanged: InverseBindingListener?
) {
    this.setOnRefreshListener {
        refreshingChanged?.onChange()
        onRefreshCommand?.invoke()
    }
}

//是否刷新中
@BindingAdapter("refrestatus")
fun SwipeRefreshLayout.setRefreshingBinding(@RefreshStatus refrestatus: Int) {
    if (refrestatus == RefreshStatus.STATUS_FORBID) {
        this.isEnabled = false
        return
    }
    this.isRefreshing = refrestatus == RefreshStatus.STATUS_REFRESHING
}

@InverseBindingAdapter(attribute = "refrestatus", event = "refreshingChanged")
fun SwipeRefreshLayout.getRefreshingBinding(): Int {
    return if (!this.isEnabled) {
        RefreshStatus.STATUS_FORBID
    } else if (this.isRefreshing) {
        RefreshStatus.STATUS_REFRESHING
    } else {
        RefreshStatus.STATUS_NONE
    }
}