package com.hzb.scaffold.databinding

import android.widget.ScrollView
import androidx.core.widget.NestedScrollView
import androidx.databinding.BindingAdapter


@BindingAdapter("onScrollChangeCommand")
fun NestedScrollView.onScrollChangeCommand(
    onScrollChangeCommand: BindCommandConsumer<NestScrollDataWrapper>
) {
    this.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
        if (onScrollChangeCommand != null) {
            onScrollChangeCommand.invoke(
                NestScrollDataWrapper(
                    scrollX,
                    scrollY,
                    oldScrollX,
                    oldScrollY
                )
            )
        }
    })
}

@BindingAdapter("onScrollChangeCommand")
fun onScrollChangeCommand(
    scrollView: ScrollView,
    onScrollChangeCommand: BindCommandConsumer<ScrollDataWrapper>
) {
    scrollView.getViewTreeObserver().addOnScrollChangedListener {
        if (onScrollChangeCommand != null) {
            onScrollChangeCommand.invoke(
                ScrollDataWrapper(
                    scrollView.scrollX.toFloat(),
                    scrollView.scrollY.toFloat()
                )
            )
        }
    }
}


class ScrollDataWrapper(var scrollX: Float, var scrollY: Float)


class NestScrollDataWrapper(
    var scrollX: Int,
    var scrollY: Int,
    var oldScrollX: Int,
    var oldScrollY: Int
)
