package com.hzb.scaffold.databinding

import android.annotation.SuppressLint
import android.text.TextUtils
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient

import android.webkit.WebView
import android.webkit.WebViewClient

import androidx.databinding.BindingAdapter


@BindingAdapter("render")
fun WebView.loadHtml(html: String?) {
    if (!TextUtils.isEmpty(html)) {
        this.loadDataWithBaseURL(null, html!!, "text/html", "UTF-8", null)
    }
}


@BindingAdapter("url", "webViewClient", "webChromeClient", requireAll = false)
fun WebView.loadContent(url: String?, webViewClient: WebViewClient?, webChromeClient: WebChromeClient?) {
    if (webViewClient != null) {
        this.webViewClient = webViewClient
    }
    this.webChromeClient = webChromeClient
    if (url != null) {
        this.loadUrl(url)
    }

}


@SuppressLint("JavascriptInterface")
@BindingAdapter("jsBridge", requireAll = false)
fun WebView.loadJsBridge(jsBridge:Map<String,Any>) {
    jsBridge.forEach { (t, u) ->
        this.addJavascriptInterface(u,t)
    }
}


