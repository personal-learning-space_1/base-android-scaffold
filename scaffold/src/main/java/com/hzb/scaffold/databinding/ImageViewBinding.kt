package com.hzb.scaffold.databinding

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.hzb.scaffold.net.glide.ImageUrl


@BindingAdapter(value = ["url", "keyUrl", "placeholderRes","bitmap","isCircle","corner"], requireAll = false)
fun ImageView.setImageUri(url: String?, keyUrl: ImageUrl?, placeholderRes: Int,bitmap: Bitmap?,isCircle:Boolean,corner:Int) {
    //使用Glide框架加载图片
    val glideBuilder = if (keyUrl != null) {
        Glide.with(this.context)
            .load(keyUrl)
    } else if (url!=null){
        Glide.with(this.context)
            .load(url)
    } else {
        Glide.with(this.context)
            .load(bitmap)
    }
    val requestOptions = if (isCircle){
        RequestOptions.circleCropTransform()
    }else if (corner>0){
        RequestOptions().transform(RoundedCorners(corner))
    }else{
        RequestOptions()
    }
    glideBuilder.apply(requestOptions.placeholder(placeholderRes))
        .into(this)
}
