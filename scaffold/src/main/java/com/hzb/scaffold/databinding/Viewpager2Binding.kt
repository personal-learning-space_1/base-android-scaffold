package com.hzb.scaffold.databinding

import androidx.databinding.BindingAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import androidx.viewpager2.widget.ViewPager2
import com.hzb.scaffold.view.recyclerview.BindingRecyclerViewAdapter
import com.hzb.scaffold.view.recyclerview.BindingRecyclerViewAdapter.ItemIds
import com.hzb.scaffold.view.recyclerview.BindingRecyclerViewAdapter.ViewHolderFactory
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding


@BindingAdapter(
    value = ["itemBinding", "items","forbidScroll" ,"adapter", "itemIds", "viewHolder"],
    requireAll = false
)
fun <T> ViewPager2.setAdapter(
    itemBinding: ItemBinding<T>?,
    items: List<T>?,
    forbidScroll:Boolean,
    adapter: BindingRecyclerViewAdapter<T>?,
    itemIds: ItemIds<in T>?,
    viewHolderFactory: ViewHolderFactory?,
) {
    var adapter = adapter
    if (itemBinding != null) {
        val oldAdapter = this.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = oldAdapter ?: BindingRecyclerViewAdapter()
        }
        adapter.itemBinding = itemBinding
        adapter.setItems(items)
        adapter.setItemIds(itemIds)
        adapter.setViewHolderFactory(viewHolderFactory)
        this.isUserInputEnabled = !forbidScroll
        if (oldAdapter !== adapter) {
            this.adapter = adapter
        }
    } else {
        this.adapter = null
    }
}

@BindingAdapter(value = ["currentIndex","smoothScroll"], requireAll = false)
fun ViewPager2.setShowIndex(currentIndex:Int,smoothScroll:Boolean=false){
    this.setCurrentItem(currentIndex,smoothScroll)
}


@BindingAdapter(
    value = ["onPageScrolledCommand", "onPageSelectedCommand", "onPageScrollStateChangedCommand"],
    requireAll = false
)
fun ViewPager2.onScrollChangeCommand(
    onPageScrolledCommand: BindCommandConsumer<ViewPagerDataWrapper>?,
    onPageSelectedCommand: BindCommandConsumer<Int>?,
    onPageScrollStateChangedCommand: BindCommandConsumer<Int>?
) {
    val callback = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
            super.onPageScrolled(position, positionOffset, positionOffsetPixels)
            onPageScrolledCommand?.invoke(
                ViewPagerDataWrapper(
                    position.toFloat(),
                    positionOffset,
                    positionOffsetPixels,
                )
            )
        }

        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
            onPageSelectedCommand?.invoke(position)
        }

        override fun onPageScrollStateChanged(state: Int) {
            super.onPageScrollStateChanged(state)
            onPageScrollStateChangedCommand?.invoke(state)
        }
    }
    this.unregisterOnPageChangeCallback(callback)
    this.registerOnPageChangeCallback(callback)
}

class ViewPagerDataWrapper(
    var position: Float,
    var positionOffset: Float,
    var positionOffsetPixels: Int,
)
