package com.hzb.scaffold.databinding

import android.widget.LinearLayout
import androidx.databinding.BindingAdapter
import com.hzb.scaffold.view.linearLayout.BindingLinearLayoutAdapter
import com.hzb.scaffold.view.linearLayout.getAdapter
import com.hzb.scaffold.view.linearLayout.setAdapter
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding

@BindingAdapter(value = ["itemBinding", "items"], requireAll = false)
fun <T> LinearLayout.setAdapterBinding(itemBinding: ItemBinding<T>?, items: List<T>?) {
    requireNotNull(itemBinding) { "itemBinding == null" }
    val oldAdapter: BindingLinearLayoutAdapter<T>? = this.getAdapter() as? BindingLinearLayoutAdapter<T>
    val adapter = oldAdapter ?: BindingLinearLayoutAdapter()
    adapter.itemBinding = itemBinding
    adapter.setItems(items)
    if (adapter != oldAdapter) {
        this.setAdapter(adapter)
    }
}