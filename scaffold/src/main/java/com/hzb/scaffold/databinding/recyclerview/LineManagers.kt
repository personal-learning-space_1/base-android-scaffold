package com.hzb.scaffold.databinding.recyclerview

import android.R
import androidx.annotation.ColorRes
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration

object LineManagers {
    fun both(
        dividerSize: Int = 1,
        @ColorRes dividerColor: Int = R.color.darker_gray
    ): LineManagerFactory {
        return object : LineManagerFactory {
            override fun create(recyclerView: RecyclerView): ItemDecoration {
                return DividerLine(
                    recyclerView.context, dividerSize,
                    dividerColor, mode = DividerLine.LineDrawMode.BOTH
                )
            }
        }
    }

    fun horizontal(
        dividerSize: Int = 1,
        @ColorRes dividerColor: Int = R.color.darker_gray
    ): LineManagerFactory {
        return object : LineManagerFactory {
            override fun create(recyclerView: RecyclerView): ItemDecoration {
                return DividerLine(
                    recyclerView.context, dividerSize,
                    dividerColor, mode = DividerLine.LineDrawMode.HORIZONTAL
                )
            }
        }
    }

    fun vertical(
        dividerSize: Int = 1,
        @ColorRes dividerColor: Int = R.color.darker_gray
    ): LineManagerFactory {
        return object : LineManagerFactory {
            override fun create(recyclerView: RecyclerView): ItemDecoration {
                return DividerLine(
                    recyclerView.context,
                    dividerSize,
                    dividerColor,
                    mode = DividerLine.LineDrawMode.VERTICAL
                )
            }
        }
    }

    interface LineManagerFactory {
        fun create(recyclerView: RecyclerView): ItemDecoration
    }
}