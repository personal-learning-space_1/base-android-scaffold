package com.hzb.scaffold.databinding

import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.databinding.BindingAdapter

typealias BindCommand = ()->Unit


typealias BindCommandConsumer<T> = (t:T)->Unit

@BindingAdapter("onClickCommand", requireAll = false)
fun View.onClickCommand(onClickCommand: BindCommand) {
    this.setOnClickListener {
        onClickCommand.invoke()
    }
}

@BindingAdapter("onLongClickCommand", requireAll = false)
fun View.onLongClickCommand(onLongClickCommand: BindCommand) {
    this.setOnLongClickListener {
        onLongClickCommand.invoke()
        true
    }
}

/**
 * view的焦点发生变化的事件绑定
 */
@BindingAdapter("onFocusChangeCommand")
fun View.onFocusChangeCommand(onFocusChangeCommand: BindCommandConsumer<Boolean>?) {
    this.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
        onFocusChangeCommand?.invoke(hasFocus)
    }
}

@BindingAdapter(value = ["isVisible"], requireAll = false)
fun isVisible(view: View, visibility: Boolean) {
    if (visibility) {
        view.visibility = View.VISIBLE
    } else {
        view.visibility = View.GONE
    }
}

@BindingAdapter(value = ["isSelect"], requireAll = false)
fun View.isSelect(isSelect: Boolean) {
    this.isSelected = isSelect
}

inline fun bindAction(crossinline command: () -> Unit): BindCommand {
    return object : BindCommand {
        override fun invoke() {
            command.invoke()
        }
    }
}
inline fun <T> bindAction(crossinline command: (value:T) -> Unit): BindCommandConsumer<T> {
    return object : BindCommandConsumer<T> {
        override fun invoke(t: T) {
            command.invoke(t)
        }
    }
}