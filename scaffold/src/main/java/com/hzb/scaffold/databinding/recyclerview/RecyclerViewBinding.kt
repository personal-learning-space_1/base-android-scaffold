package com.hzb.scaffold.databinding.recyclerview

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemAnimator
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import com.hzb.scaffold.databinding.BindCommand
import com.hzb.scaffold.databinding.BindCommandConsumer
import com.hzb.scaffold.databinding.recyclerview.LineManagers.LineManagerFactory
import com.hzb.scaffold.view.recyclerview.BindingRecyclerViewAdapter
import com.hzb.scaffold.view.recyclerview.BindingRecyclerViewAdapter.ItemIds
import com.hzb.scaffold.view.recyclerview.BindingRecyclerViewAdapter.ViewHolderFactory
import com.hzb.scaffold.view.recyclerview.PreLoadScrollListener
import com.hzb.scaffold.view.recyclerview.itembindings.ItemBinding


@BindingAdapter(
    value = ["itemBinding", "items", "adapter", "itemIds", "viewHolder"],
    requireAll = false
)
fun <T> RecyclerView.setAdapterBind(
    itemBinding: ItemBinding<in T>?,
    items: List<T>?,
    adapter: BindingRecyclerViewAdapter<T>?,
    itemIds: ItemIds<in T>?,
    viewHolderFactory: ViewHolderFactory?,
) {
    var adapter = adapter
    if (itemBinding != null) {
        val oldAdapter = this.adapter as BindingRecyclerViewAdapter<T>?
        if (adapter == null) {
            adapter = oldAdapter ?: BindingRecyclerViewAdapter()
        }


        adapter.itemBinding = itemBinding
        adapter.setItems(items)
        adapter.setItemIds(itemIds)
        adapter.setViewHolderFactory(viewHolderFactory)
        if (oldAdapter !== adapter) {
            this.adapter = adapter
        }
    } else {
        this.adapter = null
    }
}

@BindingAdapter(
    value = ["onLoadMoreCommand", "preLoadOffest", "itemDecoration"],
    requireAll = false
)
fun <T> RecyclerView.onLoadMoreCommand(
    onLoadMoreCommand: BindCommand?,
    preLoadOffest: Int?,
    itemDecoration: ItemDecoration?
) {

    if (onLoadMoreCommand != null) {
        val scrollListener = PreLoadScrollListener({ onLoadMoreCommand.invoke() }, preLoadOffest ?: 3)
        this.addOnScrollListener(scrollListener)
    }

    if (itemDecoration != null) {
        this.addItemDecoration(itemDecoration)
    }
}


@BindingAdapter(
    value = ["onScrollChangeCommand", "onScrollStateChangedCommand"],
    requireAll = false
)
fun onScrollChangeCommand(
    recyclerView: RecyclerView,
    onScrollChangeCommand: BindCommandConsumer<ScrollDataWrapper>?,
    onScrollStateChangedCommand: BindCommandConsumer<Int>?
) {
    recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        private var state = 0
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            onScrollChangeCommand?.invoke(ScrollDataWrapper(dx.toFloat(), dy.toFloat(), state))
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            state = newState
            onScrollStateChangedCommand?.invoke(newState)
        }
    })
}

@BindingAdapter("hasFixedSize", requireAll = false)
fun RecyclerView.setHasFixedSize(hasFixedSize: Boolean) {
    this.setHasFixedSize(hasFixedSize)
}


@BindingAdapter("scrollToPosition", requireAll = false)
fun RecyclerView.setScrollItem(scrollToPosition: Int) {
    this.layoutManager?.scrollToPosition(scrollToPosition)
}


@BindingAdapter("itemAnimator", requireAll = false)
fun setItemAnimator(recyclerView: RecyclerView, animator: ItemAnimator?) {
    recyclerView.itemAnimator = animator
}


class ScrollDataWrapper(var scrollX: Float, var scrollY: Float, var state: Int)
