package com.hzb.scaffold.databinding

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.BindingAdapter
import kotlinx.coroutines.flow.flow


/**
 * EditText重新获取焦点的事件绑定
 */
@BindingAdapter(value = ["requestFocus"], requireAll = false)
fun EditText.requestFocusCommand(needRequestFocus: Boolean) {
    if (needRequestFocus) {
        this.setSelection(this.getText().length)
        this.requestFocus()
        val imm =
            this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }
    this.setFocusableInTouchMode(needRequestFocus)
}


/**
 * EditText输入文字改变的监听
 */
@BindingAdapter(value = ["onTextChanged","onBeforeTextChanged","onAflterTextChanged"], requireAll = false)
fun EditText.addTextChangedListener(textChanged: BindCommandConsumer<String>?,onBeforeTextChanged: BindCommandConsumer<String>?,onAflterTextChanged: BindCommandConsumer<String>?) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
            onBeforeTextChanged?.invoke(charSequence.toString())
        }
        override fun onTextChanged(text: CharSequence, i: Int, i1: Int, i2: Int) {
            textChanged?.invoke(text.toString())
        }
        override fun afterTextChanged(editable: Editable) {
            onAflterTextChanged?.invoke(editable.toString())
        }
    })
}