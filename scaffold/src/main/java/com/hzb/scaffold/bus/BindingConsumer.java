package com.hzb.scaffold.bus;

/**
 * A one-argument action.
 *
 * @author lianyagang
 * @param <T> the first argument type
 */
public interface BindingConsumer<T> {
    /**
     * Desc:执行
     * <p>
     * Author: [lianyagang]
     * Date: 2019-12-23
     *
     * @param t 执行的脚本
     */
    void call(T t);
}
