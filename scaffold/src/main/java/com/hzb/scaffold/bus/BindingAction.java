package com.hzb.scaffold.bus;

/**
 * A zero-argument action.
 *
 * @Author: [lianyagang]
 */
public interface BindingAction {
    /**
     * Desc: 执行方法
     * <p>
     * Author: [lianyagang]
     * Date: 2019-12-20
     */
    void call();
}
