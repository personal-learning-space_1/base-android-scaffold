# BaseAndroidScaffold

#### 介绍
这是一个androd 脚手架库，快捷，方便开发

#### 配置
1. 继承BaseVMApplication:实现appScaffoldConfig() 以及logSwitch方法配置

2. ScaffoldConfig 类中所有变量都有默认值,根据需求进行定制化复制

#### 使用说明
1.状态页面的使用

```
//加载中:
statusModel.showLoading(StatusInfo(info = "数据加载中..."))

//错误:
statusModel.showError(StatusInfo(info = "发生错误"))

//空数据
statusModel.showEmpty(StatusInfo(info = "空数据"))

//正常显示
statusModel.showNormal()

//配置状态页显示区域
在activity中重写 getStatusViewControl() 方法

```

2.顶部toobar使用

```
setTopBarText("我是黄智波")
setRightText("更多")
setRightIcon()
setLeftIcon()
open fun clickRightEvent()
open fun needToobar()
```

3.状态栏设置

```
//默认子布局不延伸到状态栏,如需要可复写为true  
open fun childToEdge() = false
```

4.权限申请使用

```
PermissionLauncher.requestPermission(permiss: Array<String>,description: String? = null,grant: VoidCallback?=null,denied:VoidCallback?=null)

```




